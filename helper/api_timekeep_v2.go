package helper

import(
	//"encoding/json"
	//"fmt"
	//"log"
//		"net/http"
	//mux "../github.com/gorilla/mux"
	"fmt"
	//datatables "../datatables"
	config "../config"
	
	

)
// function route control
/*
func Timekeeping_sub_module_timekeep_v2(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	if( vars["key1"]==`assignment` ){
		Timekeep_v2_assignment(w,r)
	}
	if( vars["key1"]==`LBR_LogTotal_List` ){
		Timekeep_v2_LBR_LogTotal_List(w,r)
	}
	if( vars["key1"]==`Lbr_assign_get` ){
		Lbr_assign_get(w,r)
	}
	if( vars["key1"]==`LBR_LogRaw_get` ){
		LBR_LogRaw_get(w,r)
	}
	if( vars["key1"]==`LBR_LogPair_get` ){
		LBR_LogPair_get(w,r)
	}
	if(vars["key1"]==`LBR_TK_Pairing_Save`){
		LBR_TK_Pairing_Save(w,r)
	}
	if(vars["key1"]==`lbr_logtotal_update`){
		Lbr_logtotal_update(w,r)
	}
	if(vars["key1"]==`LBR_TK_Raw_Save`){
		LBR_TK_Raw_Save(w,r)
	}
	if(vars["key1"]==`LBR_LogPair_Delete`){
		LBR_LogPair_Delete(w,r)
	}




	

}

*/	
/*
func SQL_to_JSON(sqlstr string ,w http.ResponseWriter){
	fmt.Println(sqlstr)
	rows ,err, _,_  := config.Ap_sql(sqlstr,1)    
    

    if err != nil {
	      http.Error(w, err.Error(), http.StatusInternalServerError)
    		return
	  }
	  defer rows.Close()
	  columns, err := rows.Columns()
	  if err != nil {
	     http.Error(w, err.Error(), http.StatusInternalServerError)
    		return
	  }
	  count := len(columns)
	  tableData := make([]map[string]interface{}, 0)
	  values := make([]interface{}, count)
	  valuePtrs := make([]interface{}, count)
	  for rows.Next() {
	      for i := 0; i < count; i++ {
	          valuePtrs[i] = &values[i]
	      }
	      rows.Scan(valuePtrs...)
	      entry := make(map[string]interface{})
	      for i, col := range columns {
	          var v interface{}
	          val := values[i]
	          b, ok := val.([]byte)
	          if ok {
	              v = string(b)
	          } else {
	              v = val
	          }
	          entry[col] = v
	      }
	      tableData = append(tableData, entry)
	  }
	  //jsonData, err := json.Marshal(tableData)
	  if err != nil {
	      http.Error(w, err.Error(), http.StatusInternalServerError)
    	return
	  }
	 // fmt.Println(string(jsonData))
	  //return string(jsonData), nil 

	js, err := json.Marshal(tableData)
    
	if err != nil {
    	http.Error(w, err.Error(), http.StatusInternalServerError)
    	return
		}
  	w.Header().Set("Content-Type", "application/json")
  	//w.Header().Set("Content-Type", "application/json")
  	w.Write(js)

}

*/

func SQL_to_JSON_gin(sqlstr string) ([]map[string]interface{}) {
	fmt.Println(sqlstr)
	rows ,err, _,_  := config.Ap_sql(sqlstr,1)    
    tableData := make([]map[string]interface{}, 0)
	en := make(map[string]interface{})
    if err != nil {
		  //http.Error(w, err.Error(), http.StatusInternalServerError)
		 
		  en["Err"] = err.Error()
		  tableData = append(tableData, en)
		  return tableData
    		
	  }
	  defer rows.Close()
	  columns, err := rows.Columns()
	  if err != nil {
	     //http.Error(w, err.Error(), http.StatusInternalServerError)
		 en["Err"] = err.Error()
		 tableData = append(tableData, en)
		 return tableData
	  }
	  count := len(columns)
	  
	  values := make([]interface{}, count)
	  valuePtrs := make([]interface{}, count)
	  for rows.Next() {
	      for i := 0; i < count; i++ {
	          valuePtrs[i] = &values[i]
	      }
	      rows.Scan(valuePtrs...)
	      entry := make(map[string]interface{})
	      for i, col := range columns {
	          var v interface{}
	          val := values[i]
	          b, ok := val.([]byte)
	          if ok {
	              v = string(b)
	          } else {
	              v = val
	          }
	          entry[col] = v
	      }
	      tableData = append(tableData, entry)
	  }
	  //jsonData, err := json.Marshal(tableData)
	  if err != nil {
		fmt.Println(err) 
    //	return ''
	  }
	 // fmt.Println(string(jsonData))
	  //return string(jsonData), nil 

	return tableData
   

}

