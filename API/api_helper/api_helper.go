package api_helper

import(
	//"encoding/json"
	//"fmt"
	//"log"
	//"net/http"
	"fmt"
	//datatables "../../datatables"
	//config "../config"
	helper "../../helper"
	"github.com/gin-gonic/gin"
	"github.com/appleboy/gin-jwt"
)


func GetOrgs(c *gin.Context){
	
	//memberID := c.Request.URL.Query().Get("token") //<---- here!
	claims := jwt.ExtractClaims(c) ///decode token and extract memberID
	sqlstr := `sysaccess_get 1,` + claims["id"].(string)
	JsonRet := helper.SQL_to_JSON_gin(sqlstr);
	
	fmt.Println(JsonRet)
	
	c.JSON(200, gin.H{ "data" : JsonRet})
}

func GetAccess(c *gin.Context){
	
	//memberID := c.Request.URL.Query().Get("token") //<---- here!
	claims := jwt.ExtractClaims(c) ///decode token and extract memberID
	sqlstr := `sysaccess_get 2, `+ claims["id"].(string) + `, 1 ` 
	JsonRet := helper.SQL_to_JSON_gin(sqlstr);
	
	fmt.Println(JsonRet)
	
	c.JSON(200, gin.H{ "data" : JsonRet})
}

