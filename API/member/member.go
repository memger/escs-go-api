package member

import (
	//"encoding/json"
	//"fmt"
	//"log"
	"fmt"
	"net/http"
	//datatables "../../datatables"
	//config "../config"
	helper "../../helper"
	"github.com/gin-gonic/gin"
)

func GetMembers(c *gin.Context) {

	//memberID := c.Request.URL.Query().Get("token") //<---- here!
	//claims := jwt.ExtractClaims(c) ///decode token and extract memberID
	//sqlstr := `sysaccess_get 2, ` + claims["id"].(string) + `, 1 `

	// limit
	// query string
	// ascending	1
	// byColumn	0
	// limit	10
	// orderBy	MemberName
	// page	1
	// query
	query := c.Request.URL.Query().Get("query")
	limit := c.Request.URL.Query().Get("limit")
	page := c.Request.URL.Query().Get("page")
	//c.Request.URL.Query().Get("orderBy")

	sqlstr := `members_list 0, 0, ` + page + `, ` + limit + `, '` + query + `'`
	sqlstr_total := `members_list 1, 0, ` + page + `, ` + limit + `, '` + query + `'`

	JsonRet := helper.SQL_to_JSON_gin(sqlstr)
	JsonRet_total := helper.SQL_to_JSON_gin(sqlstr_total)

	fmt.Println(JsonRet)
	c.JSON(http.StatusOK, gin.H{"data": JsonRet, "data_total": JsonRet_total})
}
